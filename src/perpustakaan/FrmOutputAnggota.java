/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package perpustakaan;

import model.DBAnggota;
import Form.FormInputAnggotaq;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.Document;
import sun.swing.table.DefaultTableCellHeaderRenderer;

/**
 *
 * @author TRESNA BUDI
 */
public class FrmOutputAnggota extends javax.swing.JInternalFrame {

    FormInputAnggotaq input = new FormInputAnggotaq();
    DBAnggota tblAnggota = new DBAnggota();
    /**
     * Creates new form FrmOutputBarang
     */
    public FrmOutputAnggota() {
        initComponents();
        tampilkanDataAll();
    }
    
    public void tampilkanDataAll() {
        Vector<String> tableHeader = new Vector<>();
        tableHeader.add("No");
//        tableHeader.add("IDanggota");
        tableHeader.add("Nama");
        tableHeader.add("Tanggallahir");
        tableHeader.add("Telp");
        tableHeader.add("Email");
        Vector tableData = tblAnggota.Load();
        if (tableData != null) {
            jtbAnggota.setModel(new DefaultTableModel(tableData, tableHeader){
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            });
            DefaultTableCellRenderer renderer = new DefaultTableCellHeaderRenderer();
            renderer.setHorizontalAlignment(JLabel.RIGHT);
            
            //align cell table
            jtbAnggota.setModel(new DefaultTableModel(tableData, tableHeader));
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.CENTER);
            jtbAnggota.getColumnModel().getColumn(3).setCellRenderer(cellRenderer);
        }
        klikAwal();        
        jtbAnggota.setAutoCreateRowSorter(true); //mengaktifkan fitur sorting di JTable
//        jtbBarang.getTableHeader().setReorderingAllowed(false);
        //unsort table
         TableRowSorter <TableModel> sorter = new TableRowSorter <TableModel> (jtbAnggota.getModel());
         sorter.setSortable(0, false);
         sorter.addRowSorterListener(new RowSorterListener() {

             public void sorterChanged(RowSorterEvent e) {
                 int colNomor = 0;
                 for (int i = 0; i < jtbAnggota.getRowCount(); i++) {
                     jtbAnggota.setValueAt(i + 1, i, colNomor);
                 }
             }
         });
         //hide header table
        jtbAnggota.setRowSorter(sorter);
        jtbAnggota.getColumnModel().getColumn(0).setMinWidth(30);
        jtbAnggota.getColumnModel().getColumn(0).setMaxWidth(30);
        jtbAnggota.getColumnModel().getColumn(0).setWidth(0);
        jtbAnggota.getColumnModel().getColumn(0).setResizable(false);
//        jtbBarang.getColumnModel().getColumn(1).setMinWidth(0);
//        jtbBarang.getColumnModel().getColumn(1).setMaxWidth(0);
//        jtbBarang.getColumnModel().getColumn(1).setWidth(0);
//        jtbBarang.getColumnModel().getColumn(1).setResizable(false);
        jtbAnggota.grabFocus();
        klikAwal();
    }

    private void posisiPergi(int p) {
        jtbAnggota.setRowSelectionInterval(p, p);
    }

    private void klikAwal() {
        jspAnggota.getVerticalScrollBar().setValue(0);
        posisiPergi(0);
    }

    private void klikSebelum() {
        if (jtbAnggota.getSelectedRow() > 0) {
            jspAnggota.getVerticalScrollBar().setValue((jtbAnggota.getSelectedRow() - 1) * jtbAnggota.getRowHeight());
            posisiPergi(jtbAnggota.getSelectedRow()-1);
        }
    }

    private void klikSesudah() {
        if (jtbAnggota.getSelectedRow() < jtbAnggota.getRowCount() - 1) {
            jspAnggota.getVerticalScrollBar().setValue((jtbAnggota.getSelectedRow() - 1) * jtbAnggota.getHeight());
            posisiPergi(jtbAnggota.getSelectedRow() + 1);
        }
    }

    private void klikAkhir() {
        jspAnggota.getVerticalScrollBar().setValue(jtbAnggota.getRowCount() * jtbAnggota.getRowHeight());
        posisiPergi(jtbAnggota.getRowCount() - 1);
    }

    private void klikTambah() {
        input.setVisible(true);
        input.execute("");
    }

    private void klikEdit() {
        if (jtbAnggota.getSelectedRow() < 0) {
            JOptionPane.showMessageDialog(this, "Select data dulu");
        } else {
            input.setVisible(false);
            input.setSize(getWidth(), getHeight());
            String nomor = String.valueOf(jtbAnggota.getValueAt(jtbAnggota.getSelectedRow(), 0));

            input.execute(nomor);
        }
    }
    
    private void klikDelete(){
        if(jtbAnggota.getSelectedRow() < 0){
            JOptionPane.showMessageDialog(this, "Pilih Data Dulu");
        }else{
            int konfirm = JOptionPane.showConfirmDialog(null, "Hapus Data?");
            if(konfirm == JOptionPane.YES_OPTION){
                String nomor = String.valueOf(jtbAnggota.getValueAt(jtbAnggota.getSelectedRow(), 0));
                tblAnggota.delete(nomor);
                tampilkanDataAll();
//                if(jtbAnggota.getSelectedRow() < 0){
//            JOptionPane.showMessageDialog(this, "Pilih Data Dulu");
//        }else{
//            int confirm = JOptionPane.showConfirmDialog(null, "Hapus Data?");
//            if(confirm == JOptionPane.YES_OPTION){
//                ((DefaultTableModel) jtbAnggota.getModel()).removeRow(jtbAnggota.getSelectedRow());
            }
        }
    }
    
    private void klikRefresh(){
        tampilkanDataAll();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRefresh = new javax.swing.JButton();
        btnCetak = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnSesudah = new javax.swing.JButton();
        btnTambah = new javax.swing.JButton();
        btnAwal = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        btnSebelum = new javax.swing.JButton();
        jspAnggota = new javax.swing.JScrollPane();
        jtbAnggota = new javax.swing.JTable();
        btnAkhir = new javax.swing.JButton();

        setClosable(true);
        setMaximizable(true);
        setTitle("ANGGOTA");

        btnRefresh.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/represh.png"))); // NOI18N
        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnCetak.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCetak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/print.png"))); // NOI18N
        btnCetak.setText("Cetak");
        btnCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakActionPerformed(evt);
            }
        });

        btnEdit.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/edit2.png"))); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnSesudah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSesudah.setText(">");
        btnSesudah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSesudahActionPerformed(evt);
            }
        });

        btnTambah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnTambah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/plus.png"))); // NOI18N
        btnTambah.setText("Tambah");
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnAwal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAwal.setText("<<");
        btnAwal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAwalActionPerformed(evt);
            }
        });

        btnHapus.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/penghapus2.png"))); // NOI18N
        btnHapus.setText("Hapus");
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnSebelum.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSebelum.setText("<");
        btnSebelum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSebelumActionPerformed(evt);
            }
        });

        jtbAnggota.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "No", "IDanggota", "Nama", "Tanggallahir", "Harga", "Email"
            }
        ));
        jtbAnggota.setEnabled(false);
        jspAnggota.setViewportView(jtbAnggota);

        btnAkhir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAkhir.setText(">>");
        btnAkhir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAkhirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jspAnggota, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnTambah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnHapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCetak, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(btnAwal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSebelum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(67, 67, 67)
                        .addComponent(btnSesudah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAkhir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(184, 184, 184)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jspAnggota, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTambah)
                    .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCetak)
                    .addComponent(btnEdit)
                    .addComponent(btnRefresh))
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAwal)
                    .addComponent(btnSebelum)
                    .addComponent(btnSesudah)
                    .addComponent(btnAkhir))
                .addContainerGap(69, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        klikRefresh();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakActionPerformed
        Document doc = null;
        PdfPTable table = null;
        Image image = null;
        PreparedStatement ps;

        try 
        {
            print(jtbAnggota);
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(FrmOutputBuku.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }//GEN-LAST:event_btnCetakActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        klikEdit();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnSesudahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSesudahActionPerformed
        klikSesudah();
    }//GEN-LAST:event_btnSesudahActionPerformed

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        klikTambah();
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnAwalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAwalActionPerformed
        klikAwal();
    }//GEN-LAST:event_btnAwalActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        klikDelete();
    }//GEN-LAST:event_btnHapusActionPerformed

    private void btnSebelumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSebelumActionPerformed
        klikSebelum();
    }//GEN-LAST:event_btnSebelumActionPerformed

    private void btnAkhirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAkhirActionPerformed
        klikAkhir();
    }//GEN-LAST:event_btnAkhirActionPerformed

    public boolean print(JTable jt) throws Exception
    {
        //Regiter Component
        com.itextpdf.text.Document documentPdf = new com.itextpdf.text.Document(PageSize.A4.rotate());
        String desktopPath = System.getProperty("user.home") + "/Desktop" + "/Listing Data Anggota.pdf";
        PdfWriter.getInstance(documentPdf, new FileOutputStream(desktopPath.replace("\\", "/")));
        
        Rectangle One = new Rectangle(650, 400);
        documentPdf.setPageSize(One);
        
        documentPdf.setMargins(10,10,10,10);
        documentPdf.open();
        
        // Register Table
        PdfPTable table = new PdfPTable(jt.getColumnCount());
        table.setWidthPercentage(100);
        
        PdfPCell tableCell;    
        
        Paragraph textHead = new Paragraph("LISTING DATA ANGGOTA \n PERPUSKU \n", 
                FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLD, BaseColor.BLACK));
        textHead.setAlignment(Element.ALIGN_CENTER);
        documentPdf.add(textHead);
        
        Paragraph textSeparate = new Paragraph("============================================================================================", 
                FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK));
        textSeparate.setAlignment(Element.ALIGN_CENTER);
        documentPdf.add(textSeparate);
        
        Paragraph textHead_2 = new Paragraph("\n", 
                FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLD, BaseColor.BLACK));
        textHead.setAlignment(Element.ALIGN_CENTER);
        documentPdf.add(textHead_2);
        
        for(int i=0; i<jt.getColumnCount(); i++) 
        {
            Paragraph textField = new Paragraph(jt.getColumnName(i), 
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, BaseColor.WHITE));
            tableCell = new PdfPCell(textField);
            
            tableCell.setBackgroundColor(BaseColor.GREEN);
            tableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(tableCell);
        }
        
        for(int i = 0; i < jt.getRowCount(); i++) 
        {
            for (int j = 0; j < jt.getColumnCount(); j++) 
            {
                Paragraph textTransaction = new Paragraph(jt.getValueAt(i, j).toString(), 
                        FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK));
                tableCell = new PdfPCell(textTransaction);
                
                tableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(tableCell);
            }
        }
        
        float[] columnWidths = new float[]{10F,13F,10F,10F,10F,10F,10F};
        table.setWidths(columnWidths);

        documentPdf.add(table);
        documentPdf.add(textSeparate);
        documentPdf.close();
        
        JOptionPane.showMessageDialog(this, "Berhasil di Cetak");

        File myFile = new File(desktopPath.replace("\\", "/"));
        Desktop.getDesktop().open(myFile);

        return true;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAkhir;
    private javax.swing.JButton btnAwal;
    private javax.swing.JButton btnCetak;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSebelum;
    private javax.swing.JButton btnSesudah;
    private javax.swing.JButton btnTambah;
    private javax.swing.JScrollPane jspAnggota;
    private javax.swing.JTable jtbAnggota;
    // End of variables declaration//GEN-END:variables
}
